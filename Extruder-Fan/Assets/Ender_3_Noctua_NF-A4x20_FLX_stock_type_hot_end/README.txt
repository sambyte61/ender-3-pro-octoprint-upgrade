                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3601893
Ender 3 Noctua NF-A4x20 FLX stock type hot end by zuch95701 is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This is a Hot end cover to replace the stock Ender 3 or 3 pro cover. Should fit cr10 etc. It is extended in order to fit a Noctua NF-A4x20 FLX 12v fan. It's dramatically quieter and still has great air flow. Uses all of the stock hardware. 

This is a 12v fan so you will need to step down the stock voltage to match the fan. I use small buck converters for fans and such. No need for a high amperage unit. 

https://www.amazon.com/Regulator-DROK-Converter-Step-Down-Transformer/dp/B0758ZTS61

# Print Settings

Printer Brand: Creality
Printer: Ender 3 Pro
Rafts: No
Supports: Yes
Resolution: 0.2
Infill: 100

Notes: 
Remove supports from the air outlets if they auto generate there. You only need them for the tabs. 

# Post-Printing

## Post Printing

Drill out the fan standoffs to accept the standard screws with just a little bit of self threading. To tight and you will break the stand offs off. The holes for the part cooling fan will be fine. 

If part does not sit level or needs some adjustment just warm it up with a hit air gun and twist to final shape. 

# How I Designed This

From measurements on stock housing in fusion 360. If you need the step file i can provide it.