# Ender-3-Pro-Octoprint-Upgrade

## Parts

<table style="width:100%">
  <tr>
    <th>Part</th>
    <th>Cost ($)</th>
  </tr>
  <tr>
    <td>
      <a href="https://www.amazon.com/ELEMENT-Element14-Raspberry-Pi-Motherboard/dp/B07P4LSDYV/">
        <div style="height:100%;width:100%">
            Rasberry Pi 3 B+
        </div>
      </a>
    </td>
    <td>37.10</td>
  </tr>
  <tr>
    <td>
      <a href="https://www.amazon.com/Cerrxian-9Inch-Cable-Charge-2-Pack/dp/B01N5PHSJE">
        <div style="height:100%;width:100%">
            USB to USB Cable
        </div>
      </a>
    </td>
    <td>
      6.95
    </td>
  </tr>
  <tr>
    <td>
      <a href="https://www.amazon.com/Raspberry-Pi-Camera-Module-Megapixel/dp/B01ER2SKFS/">
        <div style="height:100%;width:100%">
            Raspberry Pi Camera Module V2-8 Megapixel,1080p (RPI-CAM-V2) 
        </div>
      </a>
    </td>
    <td>
      20.39
    </td>
  <tr>
    <td>
      <a href="https://www.amazon.com/gp/product/B00M4DAQH8/">
        <div style="height:100%;width:100%">
            Adafruit Flex Cable for Raspberry Pi Camera - 24" / 610mm 
        </div>
      </a>
    </td>
    <td>
      8.49
    </td>
  </tr>
  <tr>
    <td colspan="1">
      Sum
    </td>
    <td>
      77.31
    </td>
  </tr>
</table>